package main

import (
    "fmt"
    "io/ioutil"
    "log"
    "os"
    "os/exec"

    "gopkg.in/yaml.v2"
)

type AppDetails struct {
    Name              string `yaml:"name"`
    Description       string `yaml:"description"`
    LinuxPackage      string `yaml:"linux_package,omitempty"`
    LinuxPackageManager string `yaml:"linux_package_manager,omitempty"`
    Emoji             string `yaml:"emoji"`
}

type Options struct {
    OsOptions                  []AppDetails `yaml:"os_options"`
    ServerOptions              []AppDetails `yaml:"server_options"`
    DatabaseOptions            []AppDetails `yaml:"database_options"`
    ProgrammingLanguageOptions []AppDetails `yaml:"programming_language_options"`
}

func loadOptions(filename string) ([]AppDetails, error) {
    data, err := ioutil.ReadFile("options/" + filename)
    if err != nil {
        return nil, err
    }
    var opts Options

    err = yaml.Unmarshal(data, &opts)
    if err != nil {
        return nil, err
    }

    switch filename {
    case "operating_systems.yml":
        return opts.OsOptions, nil
    case "servers.yml":
        return opts.ServerOptions, nil
    case "databases.yml":
        return opts.DatabaseOptions, nil
    case "programming_languages.yml":
        return opts.ProgrammingLanguageOptions, nil
    default:
        return nil, fmt.Errorf("unknown file: %s", filename)
    }
}

func showOptions(filename string, sectionName string) []AppDetails {
    options, err := loadOptions(filename)
    if err != nil {
        log.Fatalf("error: %v", err)
    }
    fmt.Printf("Available %s options from %s:\n", sectionName, filename)
    
    for i, option := range options {
        fmt.Printf("%d. %s %s\n   Description: %s\n", i+1, option.Emoji, option.Name, option.Description)
    }

    var choice int
    fmt.Print("Select an option by number (or 0 to skip): ")
    _, err = fmt.Scanln(&choice)
    if err != nil || choice < 0 || choice > len(options) {
        fmt.Println("Invalid choice, please select again.")
        return showOptions(filename, sectionName)
    }

    if choice == 0 {
        return nil
    }

    selectedOption := options[choice-1]
    return []AppDetails{selectedOption}
}

func installPackage(packageManager, packageName string) {
    var cmd *exec.Cmd

    if packageManager == "pacman" {
        cmd = exec.Command("sudo", packageManager, "-S", packageName)
    } else {
        cmd = exec.Command("sudo", packageManager, "install", packageName, "-y")
    }

    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr

    if err := cmd.Run(); err != nil {
        fmt.Printf("Error installing package: %v\n", err)
    } else {
        fmt.Printf("Successfully installed %s\n", packageName)
    }
}

func main() {
    fmt.Println("Choose your Operating System:")
    osSelection := showOptions("operating_systems.yml", "Operating System")

    if osSelection == nil {
        fmt.Println("Operating System selection is mandatory. Exiting...")
        return
    }

    packageManager := osSelection[0].LinuxPackageManager
    fmt.Printf("Selected OS: %s (%s)\n", osSelection[0].Name, packageManager)

    fmt.Println("Choose Server Option (or 0 to skip):")
    serverSelection := showOptions("servers.yml", "Server")
    if serverSelection != nil {
        fmt.Printf("Selected Server: %s\n", serverSelection[0].Name)
        installPackage(packageManager, serverSelection[0].LinuxPackage)
    }

    fmt.Println("Choose Database Option (or 0 to skip):")
    databaseSelection := showOptions("databases.yml", "Database")
    if databaseSelection != nil {
        fmt.Printf("Selected Database: %s\n", databaseSelection[0].Name)
        installPackage(packageManager, databaseSelection[0].LinuxPackage)
    }

    fmt.Println("Choose Programming Language Option (or 0 to skip):")
    programmingLanguageSelection := showOptions("programming_languages.yml", "Programming Language")
    if programmingLanguageSelection != nil {
        fmt.Printf("Selected Programming Language: %s\n", programmingLanguageSelection[0].Name)
        installPackage(packageManager, programmingLanguageSelection[0].LinuxPackage)
    }

    fmt.Println("Setup complete.")
}
