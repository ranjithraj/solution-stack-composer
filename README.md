# Solution Stack Composer

## Overview

**Solution Stack Composer** is a Command Line Interface (CLI) tool written in Go. It is designed to load data from YAML files and provide a streamlined way to manage and compose complex solution stacks.

## Features

- **YAML File Support:** Simplifies configuration by using human-readable YAML files.
- **CLI Interface:** Easy-to-use command line interface for managing your solution stacks.
- **Interactive Mode:** Built using **Lipgloss** and **Bubble Tea**, providing a rich interactive experience directly in the terminal.
- **Efficiency:** Rapid processing of configuration files for quick deployment.

## Target Users

This tool is ideal for:

- **DevOps Engineers:** Looking to automate and manage infrastructure configurations.
- **System Administrators:** Needing an efficient way to handle multiple system configurations.
- **Developers:** Interested in organizing and deploying full-stack environments efficiently.

## Uses of This Tool

- **Configuration Management:** Automate the process of configuring servers and other infrastructure components.
- **Environment Setup:** Quickly deploy complete environments with predefined settings.
- **Template Loading:** Load and manage common configuration templates to speed up deployment processes.

## Limitations

While **Solution Stack Composer** offers many benefits, it has certain limitations that you should be aware of:

- **YAML Only:** Currently supports only YAML file formats; users needing JSON or XML support will have to convert their files.
- **Platform Compatibility:** Designed primarily for Unix-based systems; Windows support may have limitations.
- **Manual Updates:** Requires manual updates for configuration changes; no automatic detection of file changes.

## Getting Started

### Installation

To install Solution Stack Composer, follow these steps:

```sh
go install gitlab.com/ranjithraj/solution-stack-composer
```

### Usage

Here's a basic example to get started:

```sh
solution-stack-composer -config config.yaml
```

## Contribution

We welcome contributions! Please fork the repository and submit pull requests. For more information, check out our [Contribution Guidelines](./CONTRIBUTE.md).

## License

This project is licensed under the AGPLv3 License. See the full license text at [LICENSE](./LICENSE.md).

## Roadmap

For details on our future plans and upcoming features, visit our [Roadmap](https://gitlab.com/ranjithraj/solution-stack-composer/-/wikis/Roadmap).

---

For more information or detailed documentation, please visit our [GitLab Repository](https://gitlab.com/ranjithraj/solution-stack-composer).