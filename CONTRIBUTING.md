# Contributing to Solution Stack Composer

First off, thank you for taking the time to contribute! :tada: 

## How to Contribute

### Reporting Bugs

If you find a bug in the project, please create an issue using the [Issue Tracker](https://gitlab.com/ranjithraj/solution-stack-composer/-/issues). Provide detailed steps to reproduce the bug and any necessary information related to it.

### Suggesting Enhancements

If you have ideas on how to improve the project, feel free to open an issue with the suggestion tag. Describe the enhancement in detail.

### Pull Requests

We welcome contributions in the form of merge requests (MRs). Before submitting a MR, ensure you follow these steps:

1. **Fork and Clone the Repository**

   ```sh
   git clone https://gitlab.com/ranjithraj/solution-stack-composer.git
   cd solution-stack-composer
   ```

2. **Create a New Branch**

    Create a new branch for your feature or fix.

    ```sh
    git checkout -b feature-name
    ```

3. **Make Changes**

    Ensure your code adheres to the coding standards and includes appropriate comments and documentation.

4. **Test Your Changes**

    Run existing tests and add new ones where appropriate. Since the project is in Go, you can run tests using:

    ```sh
    go test ./...
    ```

5. **Commit Your Changes**

    Use clear and concise commit messages.

    ```sh
    git commit -m "Add a brief description of your changes"
    ```

6. **Push to Your Fork**

    Push your changes to your forked repository.

    ```sh
    git push origin feature-name
    ```

7. **Submit a Merge Request**

    Open a merge request against the main repository's `main` branch.

### Coding Standards

- Follow the established Go code style guidelines.
- Format your code using `go fmt`.
- Write clear and concise comments.
- Document methods and functions using proper GoDoc conventions.

### Code of Conduct

By participating in this project, you agree to abide by the [Code of Conduct](CODE_OF_CONDUCT.md).

## Resources

- [Issue Tracker](https://gitlab.com/ranjithraj/solution-stack-composer/-/issues)
- [Merge Requests](https://gitlab.com/ranjithraj/solution-stack-composer/-/merge_requests)

Thank you for contributing!

This document encourages consistent contribution practices and helps maintainers and contributors collaborate effectively.